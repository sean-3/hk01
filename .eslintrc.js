module.exports = {
  "parser": "babel-eslint",
  "extends": ["airbnb"],
  "plugins": [
    "react",
    "jsx-a11y",
    "import"
  ],
  "env": {
    "browser": true,
    "node": true,
    "es6": true,
  },
  "rules": {
    "react/destructuring-assignment":0,
    "import/no-extraneous-dependencies": 0,
    "react/jsx-filename-extension": "off",
    "react/react-in-jsx-scope": 0,
    "react/prop-types":0,
    "react/require-render-return": 2,
    "import/no-unresolved": 0,
    "jsx-a11y/label-has-associated-control":0,
    "react/prefer-stateless-function":0,
    "global-require": 0,
    "semi": 0,
    "comma-dangle": 0,
    "no-plusplus": 0,
    "prefer-destructuring": 0,
    "react/no-array-index-key": 0,
    "object-curly-newline": 0,
    "react/jsx-no-bind": 0,
    "import/prefer-default-export": 0,
    "no-return-assign": 0,
    "react/self-closing-comp": 0,
    "arrow-body-style": 0,
    "no-param-reassign": 0,
    "react/jsx-wrap-multilines": 0,
    "react/jsx-closing-tag-location": 0,
    "no-nested-ternary": 0,
    "max-len": 0,
    "react/jsx-one-expression-per-line": 0,
    "linebreak-style": [0 ,"error", "windows"]
  }
}
