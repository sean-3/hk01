# hk01 测试项目简易说明

* node版本: 10.0.0+
* 代码版本管理和CICD: gitlab
* 代码规范: ESlint(airbnb),并配合precommit和lint-staged进行提交约束
* 框架: react
* 状态管理: redux 
* UI: antd-mobile
* Lib库: request封装
* 异步 async await
* 搜索防抖: debounce-promise
* 懒加载: react-lazy-load

