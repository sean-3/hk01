import React from 'react'
import { ActivityIndicator } from 'antd-mobile';

class Loading extends React.Component {
  constructor(props) {
    super(props);
    this.state = { }
  }

  render() {
    return (
      <div style={{ textAlign: 'center' }}>
        <div style={{ padding: '10', display: 'inline-block' }}>
          <ActivityIndicator
            text="Loading..."
          />
        </div>
      </div>
    );
  }
}

export default Loading;
