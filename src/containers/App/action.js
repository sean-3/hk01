import { createActions } from 'redux-actions'
import { GET_APPS, TOP_APPS, SEARCH_APPS } from 'CONSTANTS/ActionTypes'
import * as Api from 'SERVICES/api';

const queryApps = (lists, params = {}) => {
  const { pageNo = 1, keyword } = params;
  let apps = lists
  if (keyword) {
    apps = apps.filter(item => item['im:name'].label.includes(keyword))
    return apps
  }
  const offset = (pageNo - 1) * 10;
  return apps.slice(offset, (offset + 10 >= apps.length) ? apps.length : (offset + 10))
}
const actionCreators = createActions({

  // [UPDATE_APPS]: payload => payload,
  // [GET_APPS]: payload => async () => {
  //   const res = await Api.commonApi('topfreeapplications/limit=100/json', 'GET', {});
  //   if (res && res.feed) {
  //     actionCreators.updateApps(queryApps(res.feed.entry, payload))
  //   }
  // },

  [GET_APPS]: payload => async () => {
    const res = await Api.commonApi('topfreeapplications/limit=100/json', 'GET', {});
    if (res && res.feed) {
      return queryApps(res.feed.entry, payload)
    }
    return []
  },
  [TOP_APPS]: payload => async () => {
    const res = await Api.commonApi('topgrossingapplications/limit=10/json', 'GET', payload);
    if (res && res.feed) {
      return res.feed.entry
    }
    return []
  },
  [SEARCH_APPS]: payload => async () => {
    const res = await Api.commonApi('topfreeapplications/limit=100/json', 'GET', {});
    if (res && res.feed) {
      return queryApps(res.feed.entry, payload)
    }
    return []
  }
})

export default actionCreators
