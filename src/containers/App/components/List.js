import React from 'react'
import ReactDOM from 'react-dom';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { ListView } from 'antd-mobile';
import LazyLoad from 'react-lazy-load';
import Loading from 'COMPONENTS/Loading'
import AppAction from '../action'
import './style.scss';

class List extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      dataSource: new ListView.DataSource({
        rowHasChanged: (r1, r2) => r1 !== r2
      }),
      isLoading: true,
      listHei: document.documentElement.clientHeight * 3 / 4,
      pageNo: 1,
    }
  }

  componentDidMount() {
    // eslint-disable-next-line react/no-find-dom-node
    const listHei = document.documentElement.clientHeight - ReactDOM.findDOMNode(this.lv).parentNode.offsetTop - 44;
    this.setState({ listHei }, () => {
      this.queryApp()
    });
  }

  queryApp =() => {
    const { pageNo } = this.state;
    this.props.action.getApps({ pageNo }).then(() => {
      this.setState({
        // isLoading: false,
      });
    });
  }

  onEndReached = () => {
    const { pageNo, isLoading } = this.state;
    const hasMore = pageNo <= 9
    if (isLoading || !hasMore) return
    this.setState({
      isLoading: true,
      pageNo: pageNo + 1
    }, () => {
      this.queryApp()
    })
  }

  render() {
    const renderRow = (row, i) => {
      return (
        <div key={i} className="list-item">
          <div className="name">{row['im:name'].label}</div>
          <div className="item-info">
            <LazyLoad width={79} height={64} offsetTop={24}>
              <img className="icon" src={row['im:image'][2].label} alt="" />
            </LazyLoad>
            <div style={{ flex: 1 }}>
              <div className="title">{row.title.label}</div>
              <div className="category">{row.category.attributes.label}</div>
            </div>
          </div>
        </div>
      );
    };
    const { dataSource, listHei, isLoading } = this.state
    return (
      <ListView
        ref={el => this.lv = el}
        dataSource={dataSource.cloneWithRows(this.props.reducer.apps)}
        renderFooter={() => (
          isLoading ? <Loading /> : ''
        )}
        renderRow={renderRow}
        style={{ height: listHei, overflow: 'auto' }}
        pageSize={10}
        onScroll={() => {}}
        scrollRenderAheadDistance={500}
        onEndReached={this.onEndReached}
        onEndReachedThreshold={10}
      />
    );
  }
}

const mapStateToProps = state => ({ reducer: state.AppReducer })
const mapDispatchToProps = dispatch => ({ action: bindActionCreators(AppAction, dispatch) })
export default connect(mapStateToProps, mapDispatchToProps)(List)
