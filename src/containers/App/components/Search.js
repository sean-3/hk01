import React from 'react'
import { SearchBar } from 'antd-mobile';
import Loading from 'COMPONENTS/Loading'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import debounce from 'debounce-promise'
import AppAction from '../action'


class SearchBox extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false
    }
    this.setKeyword = this.setKeyword.bind(this)
  }

  componentDidMount() {
    this.debounce = debounce((params) => {
      this.setState({
        isLoading: true
      })
      return this.props.action.searchApps(params).then(() => {
        this.setState({
          isLoading: false
        })
      })
    }, 600)
  }

  setKeyword(val) {
    this.debounce({ keyword: val })
  }

  render() {
    return (
      <div>
        <SearchBar
          placeholder="Search"
          maxLength={8}
          onChange={this.setKeyword}
        />
        {this.state.isLoading ? <Loading /> : ''}
      </div>
    );
  }
}

const mapStateToProps = state => ({ reducer: state.AppReducer })
const mapDispatchToProps = dispatch => ({ action: bindActionCreators(AppAction, dispatch) })
export default connect(mapStateToProps, mapDispatchToProps)(SearchBox)
