import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import AppAction from '../action'

class Top10 extends React.PureComponent {
  state = {
  }

  componentDidMount() {
    this.props.action.topApps();
  }

  render() {
    return (
      <div></div>
    );
  }
}
const mapStateToProps = state => ({ reducer: state.AppReducer })
const mapDispatchToProps = dispatch => ({ action: bindActionCreators(AppAction, dispatch) })
export default connect(mapStateToProps, mapDispatchToProps)(Top10)
