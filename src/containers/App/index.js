import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import Search from './components/Search';
import List from './components/List';
import AppAction from './action'


class App extends React.Component {
  componentWillMount() {
  }

  render() {
    return (
      <div>
        <Search />
        <List />
      </div>
    )
  }
}

const mapStateToProps = state => ({ reducer: state.AppReducer })
const mapDispatchToProps = dispatch => ({ action: bindActionCreators(AppAction, dispatch) })
export default connect(mapStateToProps, mapDispatchToProps)(App)
