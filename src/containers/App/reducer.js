import { handleActions } from 'redux-actions'
import { GET_APPS, TOP_APPS, SEARCH_APPS } from 'CONSTANTS/ActionTypes'

const initState = { apps: [], topten: [], keyword: '' }

export default handleActions(
  {
    [`${GET_APPS}_FULFILLED`]: (state, { payload }) => {
      return { ...state, apps: [...state.apps, ...payload] }
    },
    [`${SEARCH_APPS}_FULFILLED`]: (state, { payload }) => {
      return { ...state, apps: payload }
    },
    [`${TOP_APPS}_FULFILLED`]: (state, { payload }) => {
      return { ...state, topten: payload }
    },
  },
  initState
)
