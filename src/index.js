import React from 'react'
import ReactDOM from 'react-dom'
import { HashRouter, Switch, Route } from 'react-router-dom'
import { Provider } from 'react-redux'
import { ConnectedRouter } from 'connected-react-router'


import App from 'CONTAINERS/App'
import * as serviceWorker from './serviceWorker';
import { store, localHistory } from './store'

ReactDOM.render((
  <Provider store={store}>
    <ConnectedRouter history={localHistory}>
      <HashRouter>
        <Switch>
          <Route exact path="/" component={App} />
        </Switch>
      </HashRouter>
    </ConnectedRouter>
  </Provider>
), document.getElementById('app'))

serviceWorker.unregister();
