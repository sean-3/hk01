import request from 'UTILS/request';
import transParam2Url from 'UTILS/transParam2Url';

export const commonApi = (action, method, params = {}) => {
  if (method === 'GET') {
    return request(`/api/${action}${transParam2Url(params)}`, {
      method
    });
  }
  return request(`/api/${action}`, {
    method,
    body: JSON.stringify(params),
  });
}
