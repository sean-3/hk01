import { createStore, combineReducers, applyMiddleware, compose } from 'redux'
import { createBrowserHistory } from 'history'
import { connectRouter, routerMiddleware } from 'connected-react-router'
import promiseMiddleware from 'redux-promise-middleware'

import AppReducer from 'CONTAINERS/App/reducer'

const reducers = {
  AppReducer
}

const middleware = routerMiddleware()
const localHistory = createBrowserHistory()
// eslint-disable-next-line no-underscore-dangle
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose
const store = createStore(
  combineReducers({
    ...reducers,
    router: connectRouter(localHistory)
  }),
  composeEnhancers(applyMiddleware(middleware, promiseMiddleware))
)

export {
  store,
  localHistory
}
