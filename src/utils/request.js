import fetch from 'isomorphic-fetch';
import { Toast } from 'antd-mobile';
import NProgress from 'nprogress/nprogress';
import 'nprogress/nprogress.css';

const codeMessage = {
  200: '服务器成功返回请求的数据。',
  201: '新建或修改数据成功。',
  202: '一个请求已经进入后台排队（异步任务）。',
  204: '删除数据成功。',
  400: '发出的请求有错误，服务器没有进行新建或修改数据的操作。',
  401: '用户没有权限（令牌、用户名、密码错误）。',
  403: '用户得到授权，但是访问是被禁止的。',
  404: '发出的请求针对的是不存在的记录，服务器没有进行操作。',
  406: '请求的格式不可得。',
  410: '请求的资源被永久删除，且不会再得到的。',
  422: '当创建一个对象时，发生一个验证错误。',
  500: '服务器发生错误，请检查服务器。',
  502: '网关错误。',
  503: '服务不可用，服务器暂时过载或维护。',
  504: '网关超时。',
};
const checkStatus = (response) => {
  if (response.status >= 200 && response.status < 300) {
    return response;
  }
  const errortext = codeMessage[response.status] || response.statusText;
  Toast.offline(`请求${response.status}: ${errortext}`, 3);
  const error = new Error(errortext);
  error.name = response.status;
  error.response = response;
  throw error;
};

const parseResult = async (response) => {
  const contentType = response.headers.get('Content-Type');
  if (contentType != null) {
    // if (contentType.indexOf('text') > -1) {
    //   const resText = await response.text()
    //   return resText
    // }
    if (contentType.indexOf('form') > -1) {
      const resForm = await response.formData();
      return resForm
    }
    if (contentType.indexOf('video') > -1) {
      const resBlob = await response.blob();
      return resBlob
    }
    if (contentType.indexOf('json') > -1 || contentType.indexOf('text') > -1) {
      const resJson = await response.json()
      return resJson
    }
  }
  const resText = await response.text()
  return resText
}

const processResult = async (response) => {
  const res = await parseResult(checkStatus(response));
  return res;
}

// eslint-disable-next-line consistent-return
const request = async (url, option) => {
  const defaultOptions = {
    credentials: 'include',
  };
  const options = {
    ...option,
    headers: {
      Authorization: localStorage.getItem('token') || ''
    }
  };
  const newOptions = { ...defaultOptions, ...options };
  if (newOptions.method === 'POST' || newOptions.method === 'PUT' || newOptions.method === 'DELETE') {
    newOptions.headers = {
      Accept: 'application/json',
      ...newOptions.headers,
    };
    if (!(newOptions.body instanceof FormData)) {
      newOptions.headers['Content-Type'] = 'application/json; charset=utf-8'
    }
  }
  try {
    // NProgress.start();
    const response = await processResult(await fetch(url, newOptions));
    NProgress.done();
    return response;
  } catch (e) {
    NProgress.done();
    // 根据状态处理相应逻辑
  }
}

export default request
