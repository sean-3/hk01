export default (jsonObject) => {
  const paramsKey = Object.keys(jsonObject || {})
  const paramsKeyVal = paramsKey.filter(item => jsonObject[item]).map((key) => {
    const encodedKey = encodeURIComponent(key);
    const encodedValue = encodeURIComponent(jsonObject[key]);
    return `${encodedKey}=${encodedValue}`
  })
  return paramsKeyVal.length > 0 ? `?${paramsKeyVal.join('&')}` : ''
}
